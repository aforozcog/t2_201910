package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import model.data_structures.LinkedList;

class LinkedListTest {


	//------------------Atributos----------------------------/

	/**
	 * atributo que se enlaza con la clase que se quiere probar
	 */
	private LinkedList<Integer> numeros;

	/**
	 * escenario numero uno
	 */
	private void setUpEscenario1() {
		numeros = new LinkedList<Integer>();
	}
	
	
	/**
	 * prueba que rectifica que corra de manera correcta el 
	 */
	@Test
	public void addTest() {
		setUpEscenario1();
		numeros.add(1);
		int valor = numeros.getElementAtK(0);
		assertEquals("el numero no se a�adio de manera correcta",valor,1);
		int siz = numeros.getSize();
		assertEquals("el tama�o de la lista no es el correcto", siz, 1);
		numeros.add(3);
		valor = numeros.getElementAtK(0);
		assertEquals("el numero no se a�adio de manera correcta",valor, 3);
		siz = numeros.getSize();
		assertEquals("el tama�o de la lista no es el correcto", siz, 2);
	}
	
	/**
	 * prueba que se a�ada de manera correcta al final de la lista.
	 */
	@Test
	public void addAtEnd() {
		setUpEscenario1();
		numeros.add(1);
		numeros.addAtEnd(2);
		int valor = numeros.getElementAtK(1);
		assertEquals("el numero no se a�adio de manera correcta", valor, 2);
		int siz = numeros.getSize();
		assertEquals("el tama�o de la lista no es el correcto", 2, siz);
		setUpEscenario1();
		numeros.addAtEnd(1);
		valor = numeros.getElementAtK(0);
		assertEquals("el numero no se a�adio de manera correcta", valor, 1);
	}
	
	/**
	 * prueba que se a�ada de manera correcta en una posici�n k de la lista
	 */
	@Test
	public void AddAtKTest() {
		setUpEscenario1();
		for(int i = 0; i < 10; i++) {
			numeros.addAtEnd(i);
		}
		numeros.addAtK(10, 4);
		int valor1 = numeros.getElementAtK(5);
		int valor2 = numeros.getElementAtK(3);
		int valor3 = numeros.getElementAtK(4);
		assertEquals("la lista enlazada no quedo de la manera correcta", 10, valor3);
		assertEquals("el numero despues no es el correcto", 4, valor1);
		assertEquals("el numero antes no es el correcto", 3, valor2);
		int siz = numeros.getSize();
		assertEquals("el tama�o de la lista no es el correcto", siz, 11);
	}

	/**
	 * prueba que rectifica el funcionamiento del iterador 
	 */
	@Test
	public void IterTest() {
		numeros = new LinkedList<Integer>();
		
		for(int i = 0; i < 5; i++) {
			numeros.addAtEnd(i);
		}
		int m = 0;
		for(int ele: numeros) {
			assertEquals("no es el numero correcto en la lista", m, ele);
			m++;
		}
		m = 4;
		numeros = new LinkedList<Integer>();
		
		for(int i = 0; i < 5; i++) {
			numeros.add(i);
		}
		for(int ele: numeros) {
			assertEquals("no es el numero correcto en la lista", m, ele);
			m--;
		}
	}
	
	/**
	 * prueba que rectifica el m�todo delete
	 */
	@Test
	public void deleteTest() {
		setUpEscenario1();
		
		for(int i = 0; i < 10; i++) {
			numeros.addAtEnd(i);
		}
		/**
		 * se borra el ultimo elemento de la lista de 0 a 9
		 */
		int borrado = numeros.delete();
		
		/**
		 * se verifica que el numero borrado sea el 9 y que el nuevo largo la lista sea 9.
		 */
		int re = numeros.getSize();
		assertEquals("el elemento borrado no es el ultimo correcto", 9, borrado);
		assertEquals("el tama�o del linkedlist no disminuyo despu�s de que se borro", 9, re);
	}
	
	/**
	 * prueba que rectifica el funcionamiento de deleteAtK
	 */
	@Test
	public void deleteAtKTest() {
		setUpEscenario1();
		for(int i = 0; i < 10; i++) {
			numeros.addAtEnd(i);
		}
		int del = numeros.deleteAtK(4);
		assertEquals("el numero borrado no es el correcto", 4, del);
		int re = numeros.getSize();
		assertEquals("el tama�o del linkedlist no es el correcto", 9 ,re);
		Integer a = 5;
		Integer b = 3;
		assertEquals("la lista no quedo enlazada de la manera correcta", a, numeros.getElementAtK(4));
		assertEquals("la lista no quedo enlazada de la manera correcta", b, numeros.getElementAtK(3));
		
		/**
		 * se prueba que pueda borrar de manera correcta al comienzo y al final
		 */
		int primero = numeros.deleteAtK(0);
		int ultimo = numeros.deleteAtK(7);
		re = numeros.getSize();
		assertEquals("el elemento borrado no es el correcto", 0, primero);
		assertEquals("el elemento borrado no es el correcto", 9, ultimo);
		assertEquals("el tama�o del linkedlist no es el correcto", 7 ,re);
	}
	
	/**
	 * prueba el funcionamiento del m�todo delete tal que bora el elemento 
	 * pasado por parametro que coincida en la lista.
	 */
	@Test
	public void deleteTest2() {
		setUpEscenario1();
		for(int i = 0; i < 10; i++) {
			numeros.addAtEnd(i);
		}
		int del = numeros.delete(4);
		assertEquals("el numero borrado no es el correcto", 4, del);
		int re = numeros.getSize();
		assertEquals("el tama�o del linkedlist no es el correcto", 9 ,re);
		Integer a = 5;
		Integer b = 3;
		assertEquals("la lista no quedo enlazada de la manera correcta", a, numeros.getElementAtK(4));
		assertEquals("la lista no quedo enlazada de la manera correcta", b, numeros.getElementAtK(3));
	}
}



