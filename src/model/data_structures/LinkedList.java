package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T> {

	//--------------------Atributos------------------------------/
	
	/**
	 * primer nodo
	 */
	private Node first;
	
	/**
	 * nodo actual
	 */
	private Node actual;
	
	/**
	 * ultimo nodo
	 */
	private Node last;
	
	/**
	 * tama�o de la lista
	 */
	private int size;
	
	//-----------------------------Contructores------------------------------/
	
	/**
	 * se inicializan los nodos a null y el tama�o es cero 
	 */
	
	public LinkedList(){
		first = null;
		actual = null;
		last = null;
		size = 0;
	}
	
	/**
	 * clase Nodo que representa los nodos de la lista enlazada
	 *
	 */
	class Node{
		
		//----------------------Atributos---------------------/
		
		/**
		 * siguiente nodo al actual
		 */
		Node nex;
		
		/**
		 * nodo anterior al actual
		 */
		Node prev;
		
		/**
		 * objeto que se guarda en el arreglo
		 */
		T obje;
		
		//-----------------------------constructor-----------------/
		
		/**
		 * constructor de el nodo
		 * @param dato es el dato que se va a guardar en el nodo
		 * @param pNex siguiente nodo al que se esta creando
		 * @param pPrev nodo previo al que se esta creando
		 */
		public Node(T dato, Node pNex, Node pPrev){obje = dato; nex = pNex; prev = pPrev;}
	}

	/**
	 * metodo que a�ade un dato con un nuevo nodo al comienzo de la lista enlazada
	 * post: se a�ade al comienzo de la lista.
	 */
	public void add(T dato) {
		if(first == null){
			first = new Node(dato, null, null);
			last = first;
			actual = last;
		}
		else if(first != null){
			if(first == last) {
				first = new Node(dato,last,null);
				last.prev = first;
			}
			else {
				Node newFirst = new Node(dato,first, null);
				first.prev = newFirst;
				first = newFirst;
			}
		}
		size++;
	}

	/**
	 * se a�ade al final de la lista enlazada el nuevo dato con un nuevo nodo
	 * post: se a�ade el nuevo nodo con el dato al final de la lista enlazada
	 */
	@Override
	public void addAtEnd(T dato) {
		if(last == null){
			last = new Node(dato, null,null);
			first = last;
			actual = last;
		}
		else{
			if(first == last) {
				last = new Node(dato, null,first);
				first.nex = last;
			}
			else {
				Node newLast = new Node(dato,null, last);
				last.nex = newLast;
				last = newLast;
			}
		}
		size++;
	}

	/**
	 * Se a�ade un nuevo nodo con el dato pasado por parametro en la posici�n
	 * @param dato el dato que se va a a�adir en la posici�n
	 * @param k la posici�n en la que se va a agregar el nuevo nodo con el nuevo dato
	 * post: se ha a�adido un nuevo nodo con el dato en la posici�n k. o se ha a�adido al comiezo o al final si es el caso.
	 */
	public void addAtK(T dato, int k) {
		if(first == null || k <= 0)
			add(dato);
		else if(k >= (size - 1)) 
			addAtEnd(dato);
		else {
			Node co = first;
			for(int i = 0; i < k - 1; i++) {
				co = co.nex;
			}
			Node toAdd = new Node(dato, co.nex, co);
			co.nex.prev = toAdd;
			co.nex = toAdd;
		}
		size++;
	}

	/**
	 * regresa el elemento actual 
	 * @return elemento actual 
	 */
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		if(actual == null)return null;
		return actual.obje;
	}

	/**
	 * Regresa el tama�o de la cadena enlazada
	 * @return tama�o de la cadena enlazada
	 */
	public Integer getSize() {
		// TODO Auto-generated method stub
		return size;
	}

	/**
	 * borra el ultimo elemento de la lista enlazada
	 * post: borra el ultimo elemento de la lista
	 */
	public T delete() {
		// TODO Auto-generated method stub
		if(last == null) return null;
		T ans = last.obje;
		last = last.prev;
		last.nex = null;
		size--;
		return ans;
	}
	
	/**
	 * borra el elemento que coincida con el dato pasado por parametro
	 * @param dato dato con el que se compara
	 * post: borra el dato si se encontro
	 * @return el dato borrado
	 */
	public T delete(T dato) {
		if(size == 0)
			return null;
		if(first.obje.equals(dato)) {
			return deleteAtK(0);
		}
		Node co = first;
		while(co.nex != null) {
			if(co.nex.obje.equals(dato)) {
				T ans = co.nex.obje;
				co.nex.nex.prev = co;
				co.nex = co.nex.nex;
				size--;
				return ans;
			}
			co = co.nex;
		}
		return null;
	}

	/**
	 * borra el elemento en la posici�n k
	 * @param la posici�n del elemento que se quiere borrar
	 * pre: la posici�n tiene que ser valida
	 * post: borra el dato si se encontro
	 * @return el dato borrado
	 */
	public T deleteAtK(int k) {
		T ans;
		if(k < 0 || k > size - 1)
			return null;
		if(k == 0) {
			ans = first.obje;
			first = first.nex;
			first.prev = null;
			size--;
			return ans;
		}
		if(k == (size - 1)) {
			return delete();
		}
		Node co = first;
		for(int i = 0; i < k - 1; i++) {
			co = co.nex;
		}
		ans = co.nex.obje;
		co.nex.nex.prev = co;
		co.nex = co.nex.nex;
		size--;
		return ans;
	}

	/**
	 * devuelve el siguiente elemento al actual y cambia el actual.
	 * @return siguiente elemento al actual y cambia el actual. 
	 */
	public T next() {
		// TODO Auto-generated method stub
		if(actual.nex == null) return null;
		actual = actual.nex;
		return actual.obje;
	}

	/**
	 * devuelve el elemento anterior al actual y cambia el actual.
	 * @return elemento anterior al actual y cambia el actual.
	 */
	public T previous() {
		// TODO Auto-generated method stub
		if(actual.prev == null) return null;
		actual = actual.prev;
		return actual.obje;
	}

	
	/**
	 * devuelve el iterador del arreglo
	 */
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Ite(first);
	}

	/**
	 * devuelve el elemento en la posici�n k
	 * @return el elemento en la posici�n k si k esta entre el rango correcto
	 */
	public T getElementAtK(int k) {
		// TODO Auto-generated method stub
		if(k < 0 || k > (size -1))
			return null;
		Node co = first;
		for(int i = 0; i < k; i++) {
			co = co.nex;
		}
		
		return co.obje;
	}

	/**
	 * devuelve el elemento que coincida con el dato pasado por parametro
	 * @param objeto que se quiere comparar
	 * @return el objeto que coincide con el pasado por parametro. null si dicho elemento no existe
	 */
	public T getElement(T dato) {
		Node co = first;
		for(int i = 0; i < size; i++) {
			if(co.obje.equals(dato))
				return co.obje;
			co = co.nex;
		}
		return null;
	}
	
	/**
	 * clase que permite la creaci�n del iterador
	 */
	class Ite implements Iterator{
		Node start;
		
		public Ite(Node pStart) {
			start = pStart;
		}
		/**
		 * retorna si el elemento tiene un siguiente
		 * @return verdadeo o falso seg�n sea el caso
		 */
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return start != null;
		}

		/**
		 * retorna el siguiente elemento
		 * @return objeto t en el nodo siguiente.
		 */
		public T next() {
			// TODO Auto-generated method stub
			T ans = start.obje;
			start = start.nex;
			return ans;
		}
	}
}
