package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {

	/**
	 * a�ade el elemento a la lista
	 * @param dato un tipo generico
	 */
	public void add(T dato);
	
	/**
	 * a�ade el elemento al final de la lista
	 * @param dato
	 */
	public void addAtEnd(T dato);
	
	/**
	 * a�ade un elemento en la posici�n k
	 * @param dato un tipo generico
	 */
	public void addAtK(T dato, int k);
	
	/**
	 * regresa el elemento que coincida con dato
	 * @param dato un tipo generico
	 * @return regresa el objeto encontrado o null sino lo encontro
	 */
	public T getElement(T dato);
	
	/**
	 * regresa el elemento en la posici�n k si se encuentra
	 * @param k entero que representa la posici�n que se quiere buscar
	 * @return el elemento en dicha posici�n o null sino se encuentra.
	 */
	public T getElementAtK(int k);
	
	/**
	 * regresa el elemento en la posici�n actual de la lista enlazada.
	 * @return el elemento en la posici�n actual de la lista enlazada.
	 */
	public T getCurrentElement();
	
	/**
	 * regresa el tama�o de la lista encadenada.
	 * @return el tama�o de la lista encadenada.
	 */
	public Integer getSize();
	
	/**
	 * elimina el elemento en la posici�n
	 * @return el elemento eliminado o null si no elimino nad
	 */
	public T delete();
	
	/**
	 * elimina un elemento en la posici�n
	 * @param k la posici�n donde se elimina
	 * @return el elemento eliminado
	 */
	public T deleteAtK(int k);
	
	/**
	 * regresa el siguiente al actual y cambia el actual
	 * @return el siguiente elemento al actual
	 */
	public T next();
	
	/**
	 * regresa el elemento anterior al actual y cambia el actual
	 * @return el elemento anterior al actual
	 */
	public T previous();
}
