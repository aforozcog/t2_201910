package model.logic;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.lang.String;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

public class MovingViolationsManager implements IMovingViolationsManager {
	
	private LinkedList<VOMovingViolations> lista;
	
	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub
		
		try {
			lista = new LinkedList<VOMovingViolations>();
			File m = new File(movingViolationsFile);
			FileReader nm = new FileReader(m);
			CSVReader reader = new CSVReader(nm);
			String nextLine[];
			nextLine = reader.readNext();
			while((nextLine = reader.readNext()) != null) {
				lista.addAtEnd(new VOMovingViolations(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[13], Integer.parseInt(nextLine[9]), nextLine[12], nextLine[15],nextLine[14]));
			}
			reader.close();
		}catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	
	}

		
	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> ans = new LinkedList<VOMovingViolations>();
		for(VOMovingViolations ele: lista) {
			if(ele.getViolationCode().equals(violationCode)) ans.addAtEnd(ele);
		}
		return ans;
	}

	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> ans = new LinkedList<VOMovingViolations>();
		for(VOMovingViolations ele: lista) {
			if(ele.getAccidentIndicator().equals(accidentIndicator)) ans.addAtEnd(ele);
		}
		return ans;
	}	


}
