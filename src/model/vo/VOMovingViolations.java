package model.vo;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	//----------------------------Atributos------------------------//
	
	/**
	 * identidad de la infracci髇
	 */
	private int ID;
	
	/**
	 * lugar donde se dio la infracci髇
	 */
	private String location;
	
	/**
	 * fecha en la que se presento la infracci髇
	 */
	private String date;
	
	/**
	 * entero que expresa la suma a pagar de la infracci髇
	 */
	private int paid;
	
	/**
	 * indicador de si hubo accidente o no
	 */
	private String accidentIndicat;
	
	/**
	 * la descripci髇 de la infracci髇
	 */
	private String violationDes;
	
	/**
	 * codigo de la infracci髇
	 */
	private String violationCode;
	
	
	/**
	 * Metodo contructor, inicializa todos los atributos
	 * @param pID identificador de la multa
	 * @param pLocation lugar donde se presento la multa - no puede ser nulo
	 * @param pDate fecha en la que se dio la infracci髇 - no puede ser nulo
	 * @param pPaid cantidad que se tiene que pagar por la infracci髇 - puede ser nulo
	 * @param pAccident indicador de si hubo accidente - no puede ser nulo
	 * @param pViolaDes descripci髇 de la violaci髇 de transito - puede ser nulo
	 * @param pViolationC codigo de la violaci髇 de transito - no puede ser nulo
	 */
	public VOMovingViolations(int pID,String pLocation,String pDate,int pPaid,String pAccident,String pViolaDes, String pViolationC) {
		ID = pID;
		location = pLocation;
		date = pDate;
		paid = pPaid;
		accidentIndicat = pAccident;
		violationDes = pViolaDes;
		violationCode = pViolationC;
	}
	/**
	 * @return id - Identificador 煤nico de la infracci贸n
	 */
	public int getObjectId() {
		// TODO Auto-generated method stub
		return ID;
	}	
	
	
	/**
	 * @return location - Direcci贸n en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci贸n .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return date;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag贸 el que recibi贸 la infracci贸n en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return paid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicat;
	}
		
	/**
	 * @return description - Descripci贸n textual de la infracci贸n.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDes;
	}
	
	/**
	 * Regresa el codigo de la violaci髇 de transito.
	 * @return
	 */
	public String getViolationCode() {
		return violationCode;
	}
}
